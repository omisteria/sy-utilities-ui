import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Consumer, Contract} from '../models/models';
import {environment} from '../../environments/environment';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private http: HttpClient) { }

  getReport_expensesCurrAndLastYear(): Observable<any> {
    const url = environment.apiUrl + '/report/annual?year=2020';
    // const url = 'http://192.168.3.10:28180/api/report/annual?year=2020';
    return this.http.get<any>(url);
  }

  getReportCommonForPeriod(consumerId: number, periodDate: moment.Moment): Observable<any[]> {
    const strdate = periodDate.format('D-M-YYYY');
    const url = environment.apiUrl + `/report/common?consumerId=${consumerId}&date=${strdate}`;
    return this.http.get<any[]>(url);
  }
}
