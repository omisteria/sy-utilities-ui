import { Injectable } from '@angular/core';
import {Contract, Provider} from '../models/models';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContractsService {

  constructor(private http: HttpClient) { }

  getAllContractsForConsumer(consumerId: number): Observable<Contract[]> {
    const url = environment.apiUrl + `/contracts?consumerId=${consumerId}`;
    return this.http.get<Contract[]>(url);
  }

  getContract(contractId: number): Observable<Contract> {
    const url = environment.apiUrl + `/contract/${contractId}`;
    return this.http.get<Contract>(url);
  }

  saveContract(contract: Contract): Observable<Contract> {
    const url = environment.apiUrl + `/contract`;
    return this.http.post<Contract>(url, contract);
  }
}
