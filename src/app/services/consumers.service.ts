import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Consumer, Provider} from '../models/models';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsumersService {

  public onConsumerId;

  constructor(private http: HttpClient) {
    this.onConsumerId = new EventEmitter<number>();
  }

  getAllConsumers(): Observable<Consumer[]> {
    const url = environment.apiUrl + '/consumer/list';
    return this.http.get<Consumer[]>(url);
  }

  getConsumer(consumerId: number): Observable<Consumer> {
    const url = environment.apiUrl + `/consumer/${consumerId}`;
    return this.http.get<Consumer>(url);
  }

  saveConsumer(consumer: Consumer): Observable<Consumer> {
    const url = environment.apiUrl + `/consumer/save`;
    return this.http.post<Consumer>(url, consumer);
  }

  getCurrentConsumerId(): number {
    const val = localStorage.getItem('utility.current.consumer.id');
    if (val) {
      return parseInt(val, 0);
    }
    return NaN;
  }

  setCurrentConsumerId(consumerId: number): any {
    if (consumerId) {
      localStorage.setItem('utility.current.consumer.id', String(consumerId));
      this.onConsumerId.next(consumerId);
    }
  }
}
