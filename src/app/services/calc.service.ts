import { Injectable } from '@angular/core';
import {CalcMethod} from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class CalcService {

  methods: CalcMethod[] = [
    {id: 1, name: 'Фиксировання оплата'},
    {id: 2, name: 'Счетчик'},
    {id: 3, name: 'Оплата зависит от площади'},
    {id: 4, name: 'Оплата зависит от кол-ва проживающих'},
  ];

  constructor() { }

  getAvalableMethods(): CalcMethod[] {
    return this.methods;
  }

  // FIXSUMM(1), COUNTER(2), SQUEREBIND(3), PERSONBIND(4);
}
