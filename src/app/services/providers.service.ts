import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ConsumerService, Provider, Service} from '../models/models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  constructor(private http: HttpClient) { }

  getAllProviders(): Observable<Provider[]> {
    const url = environment.apiUrl + '/providers';
    return this.http.get<Provider[]>(url);
  }

  getProvider(providerId: number): Observable<Provider> {
    const url = environment.apiUrl + `/provider/${providerId}`;
    return this.http.get<Provider>(url);
  }

  saveProvider(provider: Provider): Observable<Provider> {
    const url = environment.apiUrl + `/provider`;
    return this.http.post<Provider>(url, provider);
  }

  deleteProvider(providerId: number): Observable<Provider> {
    const url = environment.apiUrl + `/provider/${providerId}`;
    return this.http.delete<Provider>(url);
  }

  getServices(providerId: number): Observable<Service[]> {
    const url = environment.apiUrl + `/services/${providerId}`;
    return this.http.get<Service[]>(url);
  }

  getService(serviceId: number): Observable<Service> {
    const url = environment.apiUrl + `/service/${serviceId}`;
    return this.http.get<Service>(url);
  }

  createService(service: Service): Observable<any> {
    const url = environment.apiUrl + `/service`;
    return this.http.post<any>(url, service);
  }

  updateService(consumerService: ConsumerService): Observable<any> {
    const url = environment.apiUrl + `/service`;
    return this.http.put<any>(url, consumerService);
  }
}
