import { Component, OnInit } from '@angular/core';
import {ConsumersService} from '../../services/consumers.service';
import {Consumer, CONSUMER_EMPTY} from '../../models/models';
import {BehaviorSubject, Subject} from 'rxjs';
import {DialogConsumerAddComponent} from '../../components/dialog-consumer-add/dialog-consumer-add.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-consumers',
  templateUrl: './consumers.component.html',
  styleUrls: ['./consumers.component.scss']
})
export class ConsumersComponent implements OnInit {

  dialogConfig = new MatDialogConfig();
  eventsTableReload: Subject<void> = new Subject<void>();

  constructor(public consumersService: ConsumersService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.minWidth = '700px';
  }

  onAddConsumer(): void {
    this.dialogConfig.data = Object.assign({}, CONSUMER_EMPTY);
    const dialogRef = this.dialog.open(DialogConsumerAddComponent, this.dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackBar.open('Добавлено', '', {
          duration: 3000
        });
        this.eventsTableReload.next();
      }
    });
  }

  onEditConsumer(consumerId: number): void {
    this.consumersService.getConsumer(consumerId)
      .subscribe(c => {
        this.dialogConfig.data = Object.assign({}, c);
        const dialogRef = this.dialog.open(DialogConsumerAddComponent, this.dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.snackBar.open('Изменено', '', {
              duration: 3000
            });
            this.eventsTableReload.next();
          }
        });
      });
  }
}
