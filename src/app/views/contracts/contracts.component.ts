import {Component, OnDestroy, OnInit} from '@angular/core';
import {Contract, CONTRACT_EMPTY, Provider, PROVIDER_EMPTY, Refreshable} from '../../models/models';
import {DialogProviderAddComponent} from '../../components/dialog-provider-add/dialog-provider-add.component';
import {DialogContractAddComponent} from '../../components/dialog-contract-add/dialog-contract-add.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {BehaviorSubject, Subject, Subscription} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {ContractsService} from '../../services/contracts.service';
import {ConsumersService} from '../../services/consumers.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit, OnDestroy, Refreshable {

  // contracts$ = new BehaviorSubject<Contract[]>([]);
  contractIdSubscr!: Subscription;
  eventsTableReload: Subject<void> = new Subject<void>();

  dialogConfig = new MatDialogConfig();

  constructor(private contractsService: ContractsService,
              private consumersServise: ConsumersService,
              public dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.minWidth = '700px';

    this.contractIdSubscr = this.consumersServise.onConsumerId
      .subscribe((consumerId: number) => {
        this.refresh();
      });

    // this.refresh();
  }

  ngOnDestroy(): void {
    if (this.contractIdSubscr) {
      this.contractIdSubscr.unsubscribe();
    }
  }

  refresh(): void {
    this.eventsTableReload.next();
    /*const consumerId = this.consumersServise.getCurrentConsumerId();
    if (consumerId) {
      this.contractsService.getAllContractsForConsumer(consumerId)
        .subscribe((data: Contract[]) => {
          this.contracts$.next(data);
        });
    }*/
  }

  onEditContract(contractId: number): void {
    this.contractsService.getContract(contractId)
      .subscribe(c => {
        this.dialogConfig.data = Object.assign({}, c);

        const dialogRef = this.dialog.open(DialogContractAddComponent, this.dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.snackBar.open('Изменено', '', {
              duration: 3000
            });
            this.refresh();
          }
        });
      });
  }

  onAddContract(): void {
    this.dialogConfig.data = CONTRACT_EMPTY;
    this.dialogConfig.data.consumerId = this.consumersServise.getCurrentConsumerId();
    const dialogRef = this.dialog.open(DialogContractAddComponent, this.dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackBar.open('Добавлено', '', {
          duration: 3000
        });
        this.refresh();
      }
    });
  }
}
