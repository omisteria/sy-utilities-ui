import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ProvidersService} from '../../services/providers.service';
import {DialogProviderAddComponent} from '../../components/dialog-provider-add/dialog-provider-add.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Provider, PROVIDER_EMPTY, Refreshable} from '../../models/models';
import {BehaviorSubject, Observable} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'sy-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit, Refreshable {

  providers$ = new BehaviorSubject<Provider[]>([]);

  dialogConfig = new MatDialogConfig();

  constructor(public providersService: ProvidersService,
              public dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar) {
    this.dialogConfig.disableClose = true;
    // this.dialogConfig.autoFocus = true;
    this.dialogConfig.minWidth = '700px';
  }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(): void {
    this.providersService.getAllProviders()
      .subscribe((data: Provider[]) => {
        // this.providers = { ...data };
        this.providers$.next(data);
      });
  }

  onAddService(providerId: number): void {
    this.router.navigate(['/providers', providerId, 'serviceAdd'], { relativeTo: this.route });
  }

  onEditProvider(providerId: number): void {
    const provider: Provider = this.providers$.getValue().filter(p => p.providerId === providerId)[0];
    if (provider) {
      this.dialogConfig.data = provider;
      const dialogRef = this.dialog.open(DialogProviderAddComponent, this.dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.snackBar.open('Изменено', '', {
            duration: 3000
          });
          this.refresh();
        }
      });
    }
  }

  onAddProvider(): void {
    this.dialogConfig.data = Object.assign({}, PROVIDER_EMPTY);
    const dialogRef = this.dialog.open(DialogProviderAddComponent, this.dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackBar.open('Добавлено', '', {
          duration: 3000
        });
        this.refresh();
      }
    });
  }

  onDeleteProvider(providerId: number): void {
    this.providersService.deleteProvider(providerId).subscribe(res => {
      this.snackBar.open('Удалено', '', {
        duration: 3000
      });
      this.refresh();
    });
  }
}
