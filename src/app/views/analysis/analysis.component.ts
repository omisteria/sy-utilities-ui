import { Component, OnInit } from '@angular/core';
import {ReportsService} from '../../services/reports.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss']
})
export class AnalysisComponent implements OnInit {

  constructor(private reportsService: ReportsService) { }

  ngOnInit(): void {

  }

}
