import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProvidersService} from '../../services/providers.service';
import {ConsumerService, Provider, Refreshable, Service, SERVICE_EMPTY} from '../../models/models';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-service-add',
  templateUrl: './service-add.component.html',
  styleUrls: ['./service-add.component.scss']
})
export class ServiceAddComponent implements OnInit, Refreshable {

  private providerId!: number;
  private serviceId!: number;
  public provider!: Provider;
  public service!: Service;
  public serviceName!: string;
  formGroup: FormGroup;

  constructor(
    private activateRoute: ActivatedRoute,
    private providersService: ProvidersService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.formGroup = fb.group({
      serviceName: new FormControl('', Validators.required),
      calcIndex: new FormControl(1, Validators.required),
    });
  }

  refresh(): void {
    if (this.providerId) {
      this.providersService.getProvider(this.providerId)
        .subscribe(p => {
          this.provider = p;

          if (this.serviceId) {
            this.providersService.getService(this.serviceId)
              .subscribe(s => {
                this.service = s;
                this.serviceName = s.name;
                this.formGroup.controls.serviceName.setValue(s.name);
                this.formGroup.controls.calcIndex.setValue(s.calcIndex);
              });
          }
        });
    }
  }

  ngOnInit(): void {
    this.activateRoute.params.subscribe(params => {
      this.providerId = params.providerId;
      this.serviceId = params.serviceId;

      this.refresh();
    });
  }

  submit(): void {

    if (this.service) {
      const service = Object.assign({}, this.service);
      service.name = this.formGroup.controls.serviceName.value;
      service.calcIndex = this.formGroup.controls.calcIndex.value;
      service.providerId = this.providerId;

      this.providersService.createService(service)
        .subscribe(p => {
          this.formGroup.reset();
          this.snackBar.open('Изменено', '', {
            duration: 3000
          });
          this.router.navigate(['/providers'], {relativeTo: this.route});
        });

    } else {

      const service = Object.assign({}, SERVICE_EMPTY);
      service.name = this.formGroup.controls.serviceName.value;
      service.calcIndex = this.formGroup.controls.calcIndex.value;
      service.providerId = this.providerId;

      this.providersService.createService(service)
        .subscribe(p => {
          this.formGroup.reset();
          this.snackBar.open('Добавлено', '', {
            duration: 3000
          });
          this.router.navigate(['/providers'], {relativeTo: this.route});
        });
    }
  }
}
