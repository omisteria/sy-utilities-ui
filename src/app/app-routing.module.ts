import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShortSummaryComponent} from './views/short-summary/short-summary.component';
import {ServiceSummaryComponent} from './views/service-summary/service-summary.component';
import {AnalysisComponent} from './views/analysis/analysis.component';
import {ReferencesComponent} from './views/references/references.component';
import {ContractsComponent} from './views/contracts/contracts.component';
import {ConsumersComponent} from './views/consumers/consumers.component';
import {ProvidersComponent} from './views/providers/providers.component';
import {ServiceAddComponent} from './views/service-add/service-add.component';

const routes: Routes = [
  {
    path     : 'main',
    component: ShortSummaryComponent,
    /*resolve  : {
      data: MoviesService
    }*/
  },
  {
    path     : 'analysis',
    component: AnalysisComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'contracts',
    component: ContractsComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'serviceSummary',
    component: ServiceSummaryComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'serviceSummary/:id',
    component: ServiceSummaryComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'references',
    component: ReferencesComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'consumers',
    component: ConsumersComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'providers',
    component: ProvidersComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'providers/:providerId/serviceAdd',
    component: ServiceAddComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path     : 'providers/:providerId/service/:serviceId',
    component: ServiceAddComponent,
    /*resolve  : {
      data: MovieService
    }*/
  },
  {
    path      : '**',
    redirectTo: 'main'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
