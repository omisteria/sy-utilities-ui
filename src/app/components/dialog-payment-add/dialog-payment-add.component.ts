import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'sy-dialog-payment-add',
  templateUrl: './dialog-payment-add.component.html',
  styleUrls: ['./dialog-payment-add.component.scss']
})
export class DialogPaymentAddComponent implements OnInit {

  form = new FormGroup({
    total: new FormControl(23.45),
    paymentDate: new FormControl()
  });
  // total = new FormControl(23.45);
  paymentDate = new FormControl(moment());

  constructor() { }

  ngOnInit(): void {
  }

}
