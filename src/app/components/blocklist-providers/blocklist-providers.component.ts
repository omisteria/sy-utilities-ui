import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Provider} from '../../models/models';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'sy-blocklist-providers',
  templateUrl: './blocklist-providers.component.html',
  styleUrls: ['./blocklist-providers.component.scss']
})
export class BlocklistProvidersComponent implements OnInit, OnChanges {

  @Input()
  providers$!: BehaviorSubject<Provider[]>;

  @Output() onEditProvider = new EventEmitter<number>();
  @Output() onDeleteProvider = new EventEmitter<number>();
  @Output() onAddService = new EventEmitter<number>();

  constructor() {

  }

  set providers(value: Provider[]) {
    this.providers$.next(value);
  }

  get providers(): Provider[] {
    return this.providers$.getValue();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['providers']) {
      // this.providers = [];
    }
  }

  onClickEditProvider(providerId: number): any {
    this.onEditProvider.emit(providerId);
  }

  onClickAddService(providerId: number): void {
    this.onAddService.emit(providerId);
  }

  onClickDelProvider(providerId: number): void {
    if (providerId) {
      this.onDeleteProvider.emit(providerId);
    }
  }
}
