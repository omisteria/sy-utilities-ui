import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { TableConsumerServicesDataSource, TableConsumerServicesItem } from './table-consumer-services-datasource';
import {ReportsService} from '../../services/reports.service';
import {ConsumersService} from '../../services/consumers.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-table-consumer-services',
  templateUrl: './table-consumer-services.component.html',
  styleUrls: ['./table-consumer-services.component.scss']
})
export class TableConsumerServicesComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<TableConsumerServicesItem>;
  dataSource!: TableConsumerServicesDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['providerName', 'serviceName', 'initialBalance', 'accruals', 'payment', 'finalBalance'];

  constructor(private reportService: ReportsService,
              private consumersService: ConsumersService) {
  }

  ngOnInit(): void {
    this.dataSource = new TableConsumerServicesDataSource(
      this.reportService, this.consumersService
    );
    this.reload();
  }

  ngAfterViewInit(): void {
    this.table.dataSource = this.dataSource;
  }

  reload(): void {
    this.dataSource.loadData();
  }
}
