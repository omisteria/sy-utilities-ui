import {DataSource} from '@angular/cdk/collections';
import {catchError, finalize} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {ReportsService} from '../../services/reports.service';
import {ConsumersService} from '../../services/consumers.service';
import * as moment from 'moment';

export interface TableConsumerServicesItem {
  providerName: string;
  serviceName: string;
  id: number;
  initialBalance: number;
  accruals: number;
  payment: number;
  finalBalance: number;
}



export class TableConsumerServicesDataSource extends DataSource<any> {
  private dataSubject = new BehaviorSubject<any[]>([]);

  constructor(private reportService: ReportsService,
              private consumersService: ConsumersService) {
    super();
  }

  connect(): Observable<any[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(): void {
    this.dataSubject.complete();
  }

  loadData(): void {
    this.reportService.getReportCommonForPeriod(this.consumersService.getCurrentConsumerId(),
      moment())
      .pipe(
        catchError(() => of([])),
        finalize(() => {
          // this.loadingSubject.next(false);
        })
      )
      .subscribe(data => this.dataSubject.next(data));
  }

}
