import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Consumer} from '../../models/models';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ConsumersService} from '../../services/consumers.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-dialog-consumer-add',
  templateUrl: './dialog-consumer-add.component.html',
  styleUrls: ['./dialog-consumer-add.component.scss']
})
export class DialogConsumerAddComponent implements OnInit {

  consumer: Consumer;
  formGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<DialogConsumerAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Consumer,
              fb: FormBuilder,
              private consumersService: ConsumersService) {
    this.consumer = this.data;
    this.formGroup = fb.group({
      name: new FormControl(this.consumer.name, Validators.required),
      address: new FormControl(this.consumer.address, Validators.required),
      square: new FormControl(this.consumer.square, Validators.required),
      persons: new FormControl(this.consumer.persons, Validators.required),
    });
  }

  ngOnInit(): void {
  }

  submit(): void {
    if (this.consumer) {
      this.consumer.name = this.formGroup.controls.name.value;
      this.consumer.address = this.formGroup.controls.address.value;
      this.consumer.square = this.formGroup.controls.square.value;
      this.consumer.persons = this.formGroup.controls.persons.value;

      this.consumersService.saveConsumer(this.consumer)
        .subscribe(res => {
          this.dialogRef.close(this.consumer);
        });
    }
  }
}
