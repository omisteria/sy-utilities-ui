import { Component, OnInit } from '@angular/core';
import {Color, Label} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {ReportsService} from '../../services/reports.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-chart-curr-last-year',
  templateUrl: './chart-curr-last-year.component.html',
  styleUrls: ['./chart-curr-last-year.component.scss']
})
export class ChartCurrLastYearComponent implements OnInit {
  backgroundColor1 = 'rgba(172, 184, 210, 0.7)';
  hoverBackgroundColor1 = 'rgba(172, 184, 210, 1)';
  backgroundColor2 = 'rgba(162, 152, 177, 0.7)';
  hoverBackgroundColor2 = 'rgba(162, 152, 177, 1)';

  chartData: ChartDataSets[] = [];

  chartLabels: Label[] = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

  chartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'right'
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'gray',
    },
    {
      borderColor: 'red',
    },
  ];

  chartLegend = true;
  chartPlugins = [];
  chartType: ChartType = 'bar';

  constructor(private reportsService: ReportsService) { }

  ngOnInit(): void {
    this.reportsService.getReport_expensesCurrAndLastYear()
      .subscribe(rep => {

        this.chartData = [];

        const lastYear = {
          data: rep.turnoversLast.map((t: { payments: any; }) => t.payments),
          label: '2019 год',
          fill: false,
          lineTension: 0,
          backgroundColor: this.backgroundColor2,
          hoverBackgroundColor: this.hoverBackgroundColor2,
        };
        this.chartData.push(lastYear);

        const currYear = {
          data: rep.turnoversCurr.map((t: { payments: any; }) => t.payments),
          label: '2020 год',
          fill: false,
          lineTension: 0,
          backgroundColor: this.backgroundColor1,
          hoverBackgroundColor: this.hoverBackgroundColor1,
        };
        this.chartData.push(currYear);
      });
  }

}
