import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import { MatTable } from '@angular/material/table';
import { TableConsumersDataSource } from './table-consumers-datasource';
import {ConsumersService} from '../../services/consumers.service';
import {Consumer} from '../../models/models';
import {Observable, Subscription} from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-table-consumers',
  templateUrl: './table-consumers.component.html',
  styleUrls: ['./table-consumers.component.scss']
})
export class TableConsumersComponent implements AfterViewInit, OnInit, OnDestroy {
  @Output() onEditConsumer = new EventEmitter<number>();
  @Output() onAddConsumer = new EventEmitter<void>();
  @Input() events!: Observable<void>;
  @ViewChild(MatTable) table!: MatTable<Consumer>;
  dataSource!: TableConsumersDataSource;

  displayedColumns = ['consumerId', 'name', 'address'];

  private eventsSubscription!: Subscription;

  constructor(private consumersService: ConsumersService) {
  }

  ngOnInit(): void {
    this.dataSource = new TableConsumersDataSource(this.consumersService);
    if (this.events) {
      this.eventsSubscription = this.events.subscribe(() => this.reload());
    }
    this.reload();
  }

  ngOnDestroy(): void {
    if (this.eventsSubscription) {
      this.eventsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.table.dataSource = this.dataSource;
  }

  reload(): void {
    this.dataSource.loadData();
  }

  onClickEditConsumer(consumerId: number): any {
    this.onEditConsumer.emit(consumerId);
  }

  onClickAddConsumer(): void {
    this.onAddConsumer.emit();
  }
}
