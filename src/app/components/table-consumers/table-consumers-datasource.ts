import { DataSource } from '@angular/cdk/collections';
import {catchError, finalize, map} from 'rxjs/operators';
import {Observable, BehaviorSubject, of} from 'rxjs';
import {Consumer} from '../../models/models';
import {ConsumersService} from '../../services/consumers.service';

export class TableConsumersDataSource extends DataSource<Consumer> {

  private dataSubject = new BehaviorSubject<Consumer[]>([]);

  constructor(private consumersService: ConsumersService) {
    super();
  }

  connect(): Observable<Consumer[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(): void {
    this.dataSubject.complete();
  }

  loadData(): void {
    this.consumersService.getAllConsumers()
      .pipe(
        catchError(() => of([])),
        finalize(() => {
          // this.loadingSubject.next(false);
        })
      )
      .subscribe(data => this.dataSubject.next(data));
  }
}
