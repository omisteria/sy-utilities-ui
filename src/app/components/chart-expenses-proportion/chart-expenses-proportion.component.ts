import { Component, OnInit } from '@angular/core';
import {SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, MultiDataSet} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';

@Component({
  selector: 'sy-chart-expenses-proportion',
  templateUrl: './chart-expenses-proportion.component.html',
  styleUrls: ['./chart-expenses-proportion.component.scss']
})
export class ChartExpensesProportionComponent implements OnInit {

  chartData2: ChartDataSets[] = [
    {
      data: [85, 72, 78, 75, 77, 75],
      label: 'Crude oil prices',
      fill: false,
      lineTension: 0,
      backgroundColor: 'rgba(172, 184, 210, 0.7)',
      hoverBackgroundColor: 'rgba(172, 184, 210, 1)',
    },
    {
      data: [80, 79, 77],
      label: 'Prices',
      fill: false,
      lineTension: 0,
      backgroundColor: 'rgba(162, 152, 177, 0.7)',
      hoverBackgroundColor: 'rgba(162, 152, 177, 1)',
    },
  ];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'right'
    }
  };
  public pieChartLabels: Label[] = ['SciFi', 'Drama', 'Comedy'];
  public chartData: MultiDataSet = [
    [30, 50, 20]
  ];
  chartColors = [
    {
      backgroundColor: [
        'rgba(172, 184, 210, 0.7)',
        'rgba(162, 152, 177, 0.7)',
        'rgba(162, 172, 187, 0.7)',
        'rgba(152, 152, 197, 0.7)',
        'rgba(182, 152, 177, 0.7)'
      ]
    }];

  public pieChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor() {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
  }

}
