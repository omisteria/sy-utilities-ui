import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface TableLastDataItem {
  periodName: string;
  id: number;
  initialBalance: number;
  accruals: number;
  payment: number;
  finalBalance: number;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: TableLastDataItem[] = [
  {id: 1, periodName: 'Август, 2020', initialBalance: 100.4, accruals: 10.4, payment: 80.42, finalBalance: 98.67},
  {id: 2, periodName: 'Июль, 2020', initialBalance: 100.4, accruals: 10.4, payment: 80.42, finalBalance: 98.67},
  {id: 3, periodName: 'Июнь, 2020', initialBalance: 100.4, accruals: 10.4, payment: 80.42, finalBalance: 98.67},
  {id: 4, periodName: 'Май, 2020', initialBalance: 100.4, accruals: 10.4, payment: 80.42, finalBalance: 98.67},
  {id: 5, periodName: 'Апрель, 2020', initialBalance: 100.4, accruals: 10.4, payment: 80.42, finalBalance: 98.67},
  {id: 6, periodName: 'Март, 2020', initialBalance: 100.4, accruals: 10.4, payment: 80.42, finalBalance: 98.67},
];

/**
 * Data source for the TableLastData view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class TableLastDataDataSource extends DataSource<TableLastDataItem> {
  data: TableLastDataItem[] = EXAMPLE_DATA;
  // paginator!: MatPaginator;
  sort!: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<TableLastDataItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      1 /*this.paginator.page*/,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: TableLastDataItem[]) {
    const startIndex = 1; // this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, 6/*this.paginator.pageSize*/);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: TableLastDataItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        // case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
