import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { TableLastDataDataSource, TableLastDataItem } from './table-last-data-datasource';

@Component({
  selector: 'sy-table-last-data',
  templateUrl: './table-last-data.component.html',
  styleUrls: ['./table-last-data.component.scss']
})
export class TableLastDataComponent implements AfterViewInit, OnInit {
  // @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<TableLastDataItem>;
  dataSource!: TableLastDataDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'periodName', 'initialBalance', 'accruals', 'payment', 'finalBalance'];

  ngOnInit() {
    this.dataSource = new TableLastDataDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
