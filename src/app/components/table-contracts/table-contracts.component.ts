import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { TableContractsDataSource } from './table-contracts-datasource';
import {Observable, Subscription} from 'rxjs';
import {ContractsService} from '../../services/contracts.service';
import {Contract} from '../../models/models';
import {ConsumersService} from '../../services/consumers.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-table-contracts',
  templateUrl: './table-contracts.component.html',
  styleUrls: ['./table-contracts.component.scss']
})
export class TableContractsComponent implements AfterViewInit, OnInit, OnDestroy {
  @Output() onEditContract = new EventEmitter<number>();
  @Output() onAddContract = new EventEmitter<void>();
  @Input() events!: Observable<void>;
  @ViewChild(MatTable) table!: MatTable<Contract>;
  dataSource!: TableContractsDataSource;
  private eventsSubscription!: Subscription;

  displayedColumns = ['id', 'account', 'name'];

  constructor(private contractsService: ContractsService,
              private consumersService: ConsumersService) {
  }

  ngOnInit(): void {
    this.dataSource = new TableContractsDataSource(this.contractsService, this.consumersService);
    if (this.events) {
      this.eventsSubscription = this.events.subscribe(() => this.reload());
    }
    this.reload();
  }

  ngOnDestroy(): void {
    if (this.eventsSubscription) {
      this.eventsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.table.dataSource = this.dataSource;
  }

  reload(): void {
    this.dataSource.loadData();
  }

  onClickEditContract(contractId: number): void {
    this.onEditContract.emit(contractId);
  }

  onClickAddContract(): void {
    this.onAddContract.emit();
  }
}
