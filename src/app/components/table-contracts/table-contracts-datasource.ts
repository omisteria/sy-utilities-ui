import {DataSource} from '@angular/cdk/collections';
import {MatSort} from '@angular/material/sort';
import {catchError, finalize} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {ContractsService} from '../../services/contracts.service';
import {Contract} from '../../models/models';
import {ConsumersService} from '../../services/consumers.service';


export class TableContractsDataSource extends DataSource<Contract> {
  sort!: MatSort;

  private dataSubject = new BehaviorSubject<Contract[]>([]);

  constructor(private contractsService: ContractsService,
              private consumersService: ConsumersService) {
    super();
  }

  connect(): Observable<Contract[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(): void {
    this.dataSubject.complete();
  }

  loadData(): void {
    this.contractsService.getAllContractsForConsumer(this.consumersService.getCurrentConsumerId())
      .pipe(
        catchError(() => of([])),
        finalize(() => {
          // this.loadingSubject.next(false);
        })
      )
      .subscribe(data => this.dataSubject.next(data));
  }
}
