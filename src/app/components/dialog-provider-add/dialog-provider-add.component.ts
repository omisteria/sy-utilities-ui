import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Provider, PROVIDER_EMPTY, Service} from '../../models/models';
import {ProvidersService} from '../../services/providers.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-dialog-provider-add',
  templateUrl: './dialog-provider-add.component.html',
  styleUrls: ['./dialog-provider-add.component.scss']
})
export class DialogProviderAddComponent implements OnInit {

  provider: Provider;
  formGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<DialogProviderAddComponent>,
              private providersService: ProvidersService,
              fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public data: Provider) {
    this.provider = this.data;
    this.formGroup = fb.group({
      name: new FormControl(this.provider.name, Validators.required),
    });
  }

  ngOnInit(): void {

  }

  submit(): void {
    if (this.provider) {
      this.provider.name = this.formGroup.controls.name.value;

      this.providersService.saveProvider(this.provider)
        .subscribe(res => {
          this.formGroup.reset();
          this.dialogRef.close(this.provider);
        });
    }
  }
}
