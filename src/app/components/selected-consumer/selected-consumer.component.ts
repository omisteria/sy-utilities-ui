import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';

import * as moment from 'moment';
import {Moment} from 'moment';
import {Consumer} from '../../models/models';
import {ConsumersService} from '../../services/consumers.service';

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-selected-consumer',
  templateUrl: './selected-consumer.component.html',
  styleUrls: ['./selected-consumer.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class SelectedConsumerComponent implements OnInit {

  @Input() hidePeriodDate = false;
  periodDate = new FormControl(moment());
  consumer!: Consumer;
  consumers!: Consumer[];

  formGroup!: FormGroup;

  constructor(private consumersService: ConsumersService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      consumer: [''],
      periodDate: [moment()]
    });

    this.consumersService.getAllConsumers()
      .subscribe(consumers => {
        this.consumers = consumers;

        let currConsumerId: number = this.consumersService.getCurrentConsumerId();
        if (!currConsumerId && this.consumers && this.consumers.length > 0) {
          currConsumerId = this.consumers[0].consumerId;
          this.consumersService.setCurrentConsumerId(currConsumerId);
        }

        this.formGroup.controls.consumer.setValue(currConsumerId);
      });
  }
  chosenYearHandler(normalizedYear: Moment): void {
    const ctrlValue = this.periodDate.value;
    ctrlValue.year(normalizedYear.year());
    this.periodDate.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>): void {
    const ctrlValue = this.periodDate.value;
    ctrlValue.month(normalizedMonth.month());
    this.periodDate.setValue(ctrlValue);
    datepicker.close();
  }

  onChangeConsumer(): void {
    const currConsumerId: number = this.formGroup.controls.consumer.value;
    this.consumersService.setCurrentConsumerId(currConsumerId);
  }
}
