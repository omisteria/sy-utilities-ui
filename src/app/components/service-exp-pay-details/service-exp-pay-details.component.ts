import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DialogExpensesAddComponent} from '../dialog-expenses-add/dialog-expenses-add.component';
import {DialogPaymentAddComponent} from '../dialog-payment-add/dialog-payment-add.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'sy-service-exp-pay-details',
  templateUrl: './service-exp-pay-details.component.html',
  styleUrls: ['./service-exp-pay-details.component.scss']
})
export class ServiceExpPayDetailsComponent implements OnInit {
  // @Output() voted = new EventEmitter<boolean>();

  dialogConfig = new MatDialogConfig();

  constructor(public dialog: MatDialog) {
    this.dialogConfig.disableClose = true;
    // this.dialogConfig.autoFocus = true;
    this.dialogConfig.minWidth = '700px';
  }

  ngOnInit(): void {

  }

  openDialogExpensesAdd(): void {
    const dialogRef = this.dialog.open(DialogExpensesAddComponent, this.dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openDialogPaymentAdd(): void {
    const dialogRef = this.dialog.open(DialogPaymentAddComponent, this.dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
