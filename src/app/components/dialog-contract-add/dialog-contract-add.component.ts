import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CalcMethod, Consumer, Contract, Provider, Service} from '../../models/models';
import {ConsumersService} from '../../services/consumers.service';
import {ContractsService} from '../../services/contracts.service';
import {forkJoin} from 'rxjs';
import {ProvidersService} from '../../services/providers.service';
import {CalcService} from '../../services/calc.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sy-dialog-contract-add',
  templateUrl: './dialog-contract-add.component.html',
  styleUrls: ['./dialog-contract-add.component.scss']
})
export class DialogContractAddComponent implements OnInit {

  contract: Contract;
  formGroup: FormGroup;
  consumers!: Consumer[];
  providers!: Provider[];
  services!: Service[];
  calcMethods!: CalcMethod[];

  constructor(public dialogRef: MatDialogRef<DialogContractAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Contract,
              fb: FormBuilder,
              private consumersService: ConsumersService,
              private providersService: ProvidersService,
              private contractsService: ContractsService,
              private calcService: CalcService) {
    this.contract = this.data;
    this.formGroup = fb.group({
      consumerId: new FormControl(this.contract.consumerId, Validators.required),
      providerId: new FormControl(this.contract.providerId, Validators.required),
      serviceId: new FormControl(this.contract.serviceId, Validators.required),
      account: new FormControl(this.contract.number, Validators.required),
      calcMethod: new FormControl(this.contract.calcMethod, Validators.required),
      regDate: new FormControl(this.contract.registrationDate, Validators.required),
    });
  }

  ngOnInit(): void {
    const pr1 = this.consumersService.getAllConsumers();
    const pr2 = this.providersService.getAllProviders();
    forkJoin([pr1, pr2]).subscribe(results => {
      this.consumers = results[0];
      this.providers = results[1];
    });
    this.calcMethods = this.calcService.getAvalableMethods();
    if (this.contract.providerId) {
      this.onChangedProvider(this.contract.providerId);
    }
  }

  onChangedProvider(providerId: number): void {
    this.services = [];
    this.providersService.getServices(providerId)
      .subscribe(data => {
        this.services = data;
      });
  }

  submit(): void {
    const contract = Object.assign({}, this.contract);
    contract.providerId = this.formGroup.controls.providerId.value;
    contract.serviceId = this.formGroup.controls.serviceId.value;
    contract.consumerId = this.formGroup.controls.consumerId.value;
    contract.number = this.formGroup.controls.account.value;
    contract.calcMethod = this.formGroup.controls.calcMethod.value;
    contract.registrationDate = this.formGroup.controls.regDate.value.toDate();

    this.contractsService.saveContract(contract)
      .subscribe(res => {
        this.dialogRef.close(contract);
      });
  }
}






