import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './modules/material.module';
import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {NavComponent} from './components/nav/nav.component';
import {LayoutModule} from '@angular/cdk/layout';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {ShortSummaryComponent} from './views/short-summary/short-summary.component';
import {ServiceSummaryComponent} from './views/service-summary/service-summary.component';
import {SelectedConsumerComponent} from './components/selected-consumer/selected-consumer.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TableConsumerServicesComponent} from './components/table-consumer-services/table-consumer-services.component';
import {TableLastDataComponent} from './components/table-last-data/table-last-data.component';
import {AnalysisComponent} from './views/analysis/analysis.component';
import {ReferencesComponent} from './views/references/references.component';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {ServiceToolbarComponent} from './components/service-toolbar/service-toolbar.component';
import {ServiceExpPayDetailsComponent} from './components/service-exp-pay-details/service-exp-pay-details.component';
import {DigitalLabelComponent} from './components/digital-label/digital-label.component';
import {ChartsModule} from 'ng2-charts';
import {ChartCurrLastYearComponent} from './components/chart-curr-last-year/chart-curr-last-year.component';
import {ChartExpensesProportionComponent} from './components/chart-expenses-proportion/chart-expenses-proportion.component';
import {DialogExpensesAddComponent} from './components/dialog-expenses-add/dialog-expenses-add.component';
import {DialogPaymentAddComponent} from './components/dialog-payment-add/dialog-payment-add.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ContractsComponent} from './views/contracts/contracts.component';
import {HttpClientModule} from '@angular/common/http';
import {DialogProviderAddComponent} from './components/dialog-provider-add/dialog-provider-add.component';
import {TableServicesComponent} from './components/table-services/table-services.component';
import {ServiceAddComponent} from './views/service-add/service-add.component';
import {BlocklistProvidersComponent} from './components/blocklist-providers/blocklist-providers.component';
import {ProvidersComponent} from './views/providers/providers.component';
import {ConsumersComponent} from './views/consumers/consumers.component';
import {TableConsumersComponent} from './components/table-consumers/table-consumers.component';
import {DialogConsumerAddComponent} from './components/dialog-consumer-add/dialog-consumer-add.component';
import {TableContractsComponent} from './components/table-contracts/table-contracts.component';
import {DialogContractAddComponent} from './components/dialog-contract-add/dialog-contract-add.component';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NavComponent,
    ShortSummaryComponent,
    ServiceSummaryComponent,
    SelectedConsumerComponent,
    TableConsumerServicesComponent,
    TableLastDataComponent,
    AnalysisComponent,
    ReferencesComponent,
    ServiceToolbarComponent,
    ServiceExpPayDetailsComponent,
    DigitalLabelComponent,
    ChartCurrLastYearComponent,
    ChartExpensesProportionComponent,
    DialogExpensesAddComponent,
    DialogPaymentAddComponent,
    ContractsComponent,
    DialogProviderAddComponent,
    TableServicesComponent,
    ServiceAddComponent,
    BlocklistProvidersComponent,
    ProvidersComponent,
    ConsumersComponent,
    TableConsumersComponent,
    DialogConsumerAddComponent,
    TableContractsComponent,
    DialogContractAddComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    MatDialogModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,

  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ]
})
export class AppModule { }
