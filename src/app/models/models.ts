export interface CalcMethod {
  id: number;
  name: string;
}

// === Consumer ======

export interface Consumer {
  consumerId: number;
  name: string;
  square: number;
  persons: number;
  address: string;
}
export class ConsumerEmpty implements Consumer {
  consumerId = NaN;
  name = '';
  square = 0;
  persons = 1;
  address = '';
}
export const CONSUMER_EMPTY = new ConsumerEmpty();



// === Service ======

export interface Service {
  serviceId: number;
  providerId: number;
  name: string;
  calcIndex: number;
}
export class ServiceEmpty implements Service {
  name = '';
  serviceId = NaN;
  providerId = NaN;
  calcIndex = 1;
}
export const SERVICE_EMPTY = new ServiceEmpty();

// === Provider ======

export interface Provider {
  providerId: number;
  name: string;
  services: Service[];
}

export class ProviderEmpty implements Provider {
  name = '';
  providerId = NaN;
  services = new Array<Service>();
}

export const PROVIDER_EMPTY = new ProviderEmpty();

// === Contract ======

export interface Contract {
  contractId: number;
  providerId: number;
  serviceId: number;
  consumerId: number;
  number: string;
  calcMethod: number;
  registrationDate: Date;

  provider: Provider;
  service: Service;
}

export class ContractEmpty implements Contract {
  contractId = NaN;
  providerId = NaN;
  serviceId = NaN;
  consumerId = NaN;
  number = '';
  calcMethod = 1;
  registrationDate = new Date();
  provider = PROVIDER_EMPTY;
  service = SERVICE_EMPTY;
}

export const CONTRACT_EMPTY = new ContractEmpty();

/*export interface Contract {
  consumerId: number;
  serviceId: number;
  calcMethod: string;
  consumerAccount: string;
  startService: any;
  calcRatio: number;
}*/

// Deprecated
export interface Consumption {
  serviceId: number;
  consumerId: number;
  counterId: number;
  computationMethod: string;
  consumerAccount: string;
  startService: any;
  calcRatio: number;
}

// === Counter ======

export interface CounterValue {
  counterValueId: number;
  counterId: number;
  regDate: any;
  value: number;
}

export interface Counter {
  counterId: number;
  name: string;
  checkDate: any;
  unitName: string;
  counterValue: CounterValue;
}




export interface ConsumerService {
  service: Service;
  consumption: Consumption;
  consumer: Consumer;
  counter: Counter;
}

export declare interface Refreshable {
  refresh(): void;
}
